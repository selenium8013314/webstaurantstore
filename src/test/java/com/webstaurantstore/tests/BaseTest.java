package com.webstaurantstore.tests;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestContext;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest {
  protected WebDriver driver;
  private static final String HOME_PAGE_URL = "https://www.webstaurantstore.com/";

  protected void openUrl(WebDriver driver){
    driver.get(HOME_PAGE_URL);
  }

  protected ChromeDriver startDriver(ITestContext testContext) {

    WebDriverManager.chromedriver().setup();  // Use WebDriverManager for setup

    ChromeOptions chromeOptions = new ChromeOptions();

    chromeOptions.addArguments("--disable-notifications", "--ignore-certificate-errors", "--disable-extensions");
    chromeOptions.addArguments("disable-infobars");
    chromeOptions.addArguments("disable-dev-shm-usage");
    chromeOptions.addArguments("disable-features=VizDisplayCompositor");
    chromeOptions.addArguments("disable-features=IsolateOrigins,site-per-process");
    chromeOptions.addArguments("no-sandbox");
    chromeOptions.addArguments("disable-popup-blocking");
    chromeOptions.addArguments("enable-javascript");

    chromeOptions.setExperimentalOption("useAutomationExtension", false);
    chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
    chromeOptions.setExperimentalOption("prefs", Map.of(
            "profile.default_content_setting_values.notifications", 2,
            "profile.default_content_setting_values.geolocation", 1,
            "credentials_enable_service", false,
            "profile.password_manager_enabled", false));

    driver = new ChromeDriver(chromeOptions);

    testContext.setAttribute("webDriver", driver);

    driver.manage().window().maximize();
    driver.manage().deleteAllCookies();

    return (ChromeDriver) driver;
  }


}
