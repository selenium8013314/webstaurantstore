package com.webstaurantstore.utils;

public enum LocatorType {
    CSS_SELECTOR,
    NAME,
    XPATH,
    ID,
    CLASS_NAME,
    TAG_NAME,
    LINK_TEXT,
    PARTIAL_LINK_TEXT

}



