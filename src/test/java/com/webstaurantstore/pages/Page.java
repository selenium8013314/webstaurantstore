package com.webstaurantstore.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.webstaurantstore.utils.LocatorType;

import java.util.List;

/**
 * Abstract class representation of a Page in the UI. Page object pattern
 */
public abstract class Page {

  protected WebDriver driver;

  /*
   * Constructor injecting the WebDriver interface
   *
   * @param webDriver
   */
  public Page(WebDriver driver) {
    this.driver = driver;
  }

  public WebElement getElement(LocatorType locatorType, String LocatorString) {
    WebElement el = null;

    try {
      switch (locatorType) {
        case CSS_SELECTOR:
          el = driver.findElement(By.cssSelector(LocatorString));
          break;
        case NAME:
          el = driver.findElement(By.name(LocatorString));
          break;
        case XPATH:
          el = driver.findElement(By.xpath(LocatorString));
          break;
        case ID:
          el = driver.findElement(By.id(LocatorString));
          break;
        case CLASS_NAME:
          el = driver.findElement(By.className(LocatorString));
          break;
        case TAG_NAME:
          el = driver.findElement(By.tagName(LocatorString));
          break;
        case LINK_TEXT:
          el = driver.findElement(By.linkText(LocatorString));
          break;
        default:
          el = driver.findElement(By.partialLinkText(LocatorString));
          break;
      }
      return el;
    }
    catch (NoSuchElementException e) {
      System.out.println("No Open Cart Button found");
      return el;
    }
  }

  public List<WebElement> getElements(LocatorType locatorType, String LocatorString) {
    List<WebElement> el = null;

    try {
      switch (locatorType) {
        case CSS_SELECTOR:
          el = driver.findElements(By.cssSelector(LocatorString));
          break;
        case NAME:
          el = driver.findElements(By.name(LocatorString));
          break;
        case XPATH:
          el = driver.findElements(By.xpath(LocatorString));
          break;
        case ID:
          el = driver.findElements(By.id(LocatorString));
          break;
        case CLASS_NAME:
          el = driver.findElements(By.className(LocatorString));
          break;
        case TAG_NAME:
          el = driver.findElements(By.tagName(LocatorString));
          break;
        case LINK_TEXT:
          el = driver.findElements(By.linkText(LocatorString));
          break;
        default:
          el = driver.findElements(By.partialLinkText(LocatorString));
          break;
      }
      return el;
    }
    catch (NoSuchElementException e) {
      System.out.println("No Open Cart Button found");
      return el;
    }

  }


  public String getTitle() {
    return driver.getTitle();
  }



}