# WebstaurantStore Test Automation Framework

This is a test automation framework for WebstaurantStore.com.
## Frameworks and tools used:
- Java 14
- Maven
- Selenium
- TestNG

# Instructions
1. Modify CHROME_VERSION in webstaurantstore.src.test.org.webstaurant.pages.BaseTest with the current Chrome Version.

2. Run: mvn test

## ToDo

* Basic test structure implemented
    * **To Do:*
        * safari(stop working after MacOS upgrade)
        * Sometimes following error is displayed (add validation when it appears):
          selenium.common.exceptions.ElementClickInterceptedException:
          Message: Element &lt;span id="cartItemCountSpan" class="bg-white rounded-tr rounded-br flex
          items-center font-bold text-base leading-5 px-2 text-gray-800 justify-center box-border md:border-gray-400
          md:border md:border-l-0 md:border-solid h-[30px] md:h-[34px] lt:h-10 tracking-[.02em]"&gt;
          is not clickable at point (1884,70) because another element &lt;div class="notification__description"&gt; obscures it
          Stacktrace:
          RemoteError@chrome://remote/content/shared/RemoteError.sys.mjs:8:8
          WebDriverError@chrome://remote/content/shared/webdriver/Errors.sys.mjs:193:5
          ElementClickInterceptedError@chrome://remote/content/shared/webdriver/Errors.sys.mjs:337:5
          webdriverClickElement@chrome://remote/content/marionette/interaction.sys.mjs:177:11
          interaction.clickElement@chrome://remote/content/marionette/interaction.sys.mjs:136:11
          clickElement@chrome://remote/content/marionette/actors/MarionetteCommandsChild.sys.mjs:205:29
          receiveMessage@chrome://remote/content/marionette/actors/MarionetteCommandsChild.sys.mjs:85:31
        * Pytest: tests/search_test.py::TestSearchItems::test_verify_items[stainless work table] 
          The chromedriver version (128.0.6613.86) detected in PATH at /opt/homebrew/bin/chromedriver might not be 
          compatible with the detected chrome version (129.0.6668.100); currently, chromedriver 129.0.6668.100 
          is recommended for chrome 129.*, so it is advised to delete the driver in PATH and retry
        * Remote: Selenium Grid
            * Cross Browser Testing (Remote) 
        * Parallel Testing: Cucumber
        * Add Video recordings: Only works for remote - Pytest
        * Add Unittests: mockito
        * Sauce labs
        * Cucumber: create reports using net.masterthought
        * TestNG: Improved reports
      
  
    * To Review:
      * Test Data Management: Consider using data providers or external data sources for test data. 
      * Error Handling: Implement proper error handling to make the test more robust.
